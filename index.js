const puppeteer = require('puppeteer');
const fs = require('fs');
const util = require('util');

//Lo utilizamos para la apertura asincronica del archivo utilizando promisify
const readFile = (fileName) => util.promisify(fs.readFile)(fileName, 'utf8');

/**
 * Clase implementada para trabajar con las fechas.
 */
class Fecha extends Date{

	getFechaDDMMYYYY(){
		return this.getDate() + "/"+ (this.getMonth()+1)+"/" +this.getFullYear();
	}

	getHoraHHMM(){
		return this.getHours()+":"+(this.getMinutes() < 10 ? '0' : '') + this.getMinutes();
	}

}

class P12Article{
	constructor(title, url, dateTimeExtraccion) {
		this.title = title;
		this.url = url;
		this.date = null;
		this.dateTimeExtraccion = dateTimeExtraccion;
	}

	static async _getInitializeData(page, DOMElement){
		try{
			let url = await page.evaluate(el => el.getElementsByTagName("a")[0].href, DOMElement);
			let title = await page.evaluate(el => el.innerText, DOMElement);
			//let innerText = await DOMElement.evaluate(node => node.innerText);
			return {url, title}
		}
		catch(e){
			console.log("Error obtaining news data: " + e);
		}
	}

	static async build(DOMElement, browser, page, dateTimeExtraccion) {
		try{
			let article_data = await P12Article._getInitializeData(page, DOMElement);
			let article = new P12Article(article_data.title, article_data.url, dateTimeExtraccion);
			await article.setDate(browser);
			return article;
		}
		catch(e){
			console.log("Error building instance: " + e);
			return null;
		}
	}

	async setDate(browser){
		let page = await browser.newPage();
		try{
			await page.goto(this.url, {waitUntil:'networkidle2'});
			let timeNode = (await page.$$(".time"))[0];
			let extractedTime =  await page.evaluate(el => el.innerText, timeNode);
			this.date = extractedTime;
		}catch{
			console.log("Can't extract article's date:" +  e);
		}
		finally{
			await page.close();
			return this.date;
		}
	}

	displayConsole(nuevo){
		if (nuevo==true){
			console.log("██████ NUEVO ██████");
		}else{
			let fechaExtraccion = new Fecha(this.getDateTimeExtraccion());
			console.log("══════ EXTRACCION - Fecha: "+fechaExtraccion.getFechaDDMMYYYY() + " Hora: "+fechaExtraccion.getHoraHHMM());
		}
		console.log("	"+this.getTitle());
		console.log("	"+this.getDate());
		console.log("------------------------------------------------------");
	}

	getDate(){
		return this.date;
	}

	getTitle(){
		return this.title;
	}

	getDateTimeExtraccion(){
		return this.dateTimeExtraccion;
	}
}

class ExtractionTemplate{
	constructor(json){
		this.base_url = json.url;
		this.selector = json.selector;
		this.domainClass = json.domainClass;
	}
}

class Scraper{
	async scrap(template, cant = 1){
		let url = template.url;
		let browser = await puppeteer.launch();
		let page = await browser.newPage();
		await page.goto(url, {waitUntil:'networkidle2'});
		let DOMElements = await page.$$(template.selector);
		let domainElements = [];

		//Se considera la fecha Y hora de extraccion el momento en que se scrapean todos
		let fechaHoraExtraccion = new Date();

		for(let DOMElement of DOMElements){

			try{
				let domainObject = await template.domainClass.build(DOMElement, browser, page, fechaHoraExtraccion);

				if (domainObject == null) continue;
				domainElements.push(domainObject);
				if (domainElements.length >= cant) break;
			}catch(e){
				console.log("Can't scrap article:" +  e);
			}
		};
		await browser.close();
		return domainElements;
	}

	/**
	 * @description Recibe la coleccion de articulos scrapeados y los procesa para almacenar en archivo.
	 * Solo se adhicionan los articulos que no existian previamiente.
	 * @method procesarArticulos
	 * @author Javier Marchesini
	*/
	async procesarArticulos(articles){
		let data = null;
		let totalNoticias= articles.length;
		let totalNoticiaNuevas = 0;

		console.log( " █████╗ ██████╗ ████████╗██╗ ██████╗██╗   ██╗██╗      ██████╗ ███████╗    ███╗   ██╗██╗   ██╗███████╗██╗   ██╗ ██████╗ ███████╗");
		console.log( "██╔══██╗██╔══██╗╚══██╔══╝██║██╔════╝██║   ██║██║     ██╔═══██╗██╔════╝    ████╗  ██║██║   ██║██╔════╝██║   ██║██╔═══██╗██╔════╝");
		console.log( "███████║██████╔╝   ██║   ██║██║     ██║   ██║██║     ██║   ██║███████╗    ██╔██╗ ██║██║   ██║█████╗  ██║   ██║██║   ██║███████╗");
		console.log( "██╔══██║██╔══██╗   ██║   ██║██║     ██║   ██║██║     ██║   ██║╚════██║    ██║╚██╗██║██║   ██║██╔══╝  ╚██╗ ██╔╝██║   ██║╚════██║");
		console.log( "██║  ██║██║  ██║   ██║   ██║╚██████╗╚██████╔╝███████╗╚██████╔╝███████║    ██║ ╚████║╚██████╔╝███████╗ ╚████╔╝ ╚██████╔╝███████║");
		console.log( "╚═╝  ╚═╝╚═╝  ╚═╝   ╚═╝   ╚═╝ ╚═════╝ ╚═════╝ ╚══════╝ ╚═════╝ ╚══════╝    ╚═╝  ╚═══╝ ╚═════╝ ╚══════╝  ╚═══╝   ╚═════╝ ╚══════╝");
		console.log( " ══════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════");
		console.log( "                                                                                                                               ");
		//Verificamos si el archivo existe
		if (fs.existsSync(template.file)) {
			// Leemos el archivo ya existente utilizando el componente promisify de Node y luego lo parseamos a un object array.
			let fileRead = await readFile(template.file);
			let articlesArray = JSON.parse(fileRead);

			articles.forEach(article => {
				//Buscamos si la noticia ya existe en el archivo. Utilizamos el campo URL para la busqueda
				let casos = articlesArray.find(e => e.url === article.url);
				if (casos===undefined){
					totalNoticiaNuevas++;
					articlesArray.push(article);
					article.displayConsole(true);

				}else{
					article.displayConsole(false);

				}
			});

			data = articlesArray;
		}else{
			//Si el archivo no existe se van a almacenar todos los articulos.
			for(let article of articles){
				article.displayConsole(true);


			}
			totalNoticiaNuevas=totalNoticias;
			data = articles;
		}

		//Escribimos los datos al archivo.
		await fs.writeFileSync(template.file, JSON.stringify(data));

		let resultado = {
							totalNoticias: totalNoticias,
							totalNoticiaNuevas: totalNoticiaNuevas
						}
		return resultado
	}

}

let template = {
	url:'https://www.pagina12.com.ar',
	selector:'article',
	domainClass:P12Article,
	file:"P12Articles.json"
};

let scraper = new Scraper(template);
scraper.scrap(template, 10).then(async (articles) => {
								try{
									//Procesamos los articulos obtenidos
									let data = await scraper.procesarArticulos(articles);

									let fecha = new Fecha();

									console.log("═════════════════════════════════════════════════════");
									console.log("RESUMEN      | Fecha: "+fecha.getFechaDDMMYYYY() + " Hora: "+fecha.getHoraHHMM() );
									console.log("═════════════════════════════════════════════════════");
									console.log("   Total Noticias:        			"+data.totalNoticias);
									console.log("   Total Noticias Nuevas: 			"+data.totalNoticiaNuevas);
							}catch(e){
									console.log("Error procesando archivo: " + e);
								}
							});


