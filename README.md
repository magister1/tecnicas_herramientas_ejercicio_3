# Ejercicio N° 3 -  Scraping con Puppeteer.js 
### Técnicas y Herramientas 2020 | Maestría en Ingeniería de Software | Universidad Nacional de La Plata

### Instalación

El script requiere [Node.js](https://nodejs.org/) v12+.
Instalar dependencias 

```sh
$ cd tecnicas_herramientas_ejercicio_3
$ npm install -d
$ node index.js
```

### Dependencias
Las siguientes dependencias son necesarias para el proyecto.

| Dependencia | README |
| ------ | ------ |
| Puppeteer | [https://github.com/puppeteer/puppeteer][puppeteer] |

  [node.js]: <http://nodejs.org>
  [puppeteer]: <https://github.com/puppeteer/puppeteer/blob/master/README.md>
 
